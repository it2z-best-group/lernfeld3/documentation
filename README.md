# Dokumentation

![logo](./src/logo.png)

## Inhaltsverzeichnis

1. [Einleitung](#einleitung)
2. [Projekthintergrund](#projekthintergrund)
   1. [Vorstellung des eigenen Unternehmens](#vorstellung-des-eigenen-unternehmens)
   2. [Vorstellung des Kunden](#vorstellung-des-kunden)
   3. [Projektauftrag](#projektauftrag)
3. [Projektplanung](#projektplanung)
   1. [Softwarewahl](#software---betriebssystemwahl)
4. [Projektrealisierung](#projektrealisierung)
   1. [Installation und Konfiguration der Hardware](#installation-und-konfiguration-der-hardware)
   2. [Installation und Konfiguration der Software](#installation-und-konfiguration-der-software)
5. [Anhang](#anhang)
   1. [Quellenverzeichnis](#quellenverzeichnis)
   2. [Grafiken](#grafiken)

## Einleitung

Diese Dokumentation beschreibt die Planung, die Materialbeschaffung, die Vorgehensweise und den Aufbau / die Struktur des Netzwerkes für den Kunden GEEK-Fitness GmbH.

## Projekthintergrund

### Vorstellung des eigenen Unternehmens

Die SUPER-IT Solutions GmbH befasst sich hauptsächlich mit Großprojekten zum Thema Netzwerke.  
Dies beinhaltet die Planung, den Aufbau, die Inbetriebnahme und die Wartung von Netzwerken und die Betreuung / Beratung der Netzwerknutzer.

### Vorstellung des Kunden

Die GEEK-Fitness GmbH ist ein großes, deutschlandweit bekanntes Fitnessunternehmen mit Hauptsitz in Hamburg. Das Unternehmen wurde von der großen, internationalen Fitnesskette GENIUS Ltd. gekauft und soll nun neu strukturiert werden.

### Projektauftrag

Die GEEK-Fitness GmbH soll auf Wunsch des neuen Eigentümers neu strukturiert werden. Alle Abteilungen des Hauptgebäudes der GEEK-Fitness GmbH sind miteinander vernetzt. Die einzelnen Abteilungen sollen zum Teil mit zusätzlichen PC-Arbeitsplätzen ausgestattet werden.

Der Kunde wünscht sich:

- Einen Netzwerkplan
- Ein IPv4 Adressierungsschema
- Eine Switchkonfiguration inkl. Portbelegung

In "Szenario X" wird das Netzwerk von Grund auf von der SUPER-IT Solutions GmbH konzeptioniert / aufgebaut / konfiguriert / getestet / dokumentiert.

Spezifisch:

- Firewall einrichten mit OpenWRT
- Webserver
- DHCP-Dienst mit Adressbereich von 172.16.7.20 - 172.16.7.30
- DNS-Dienst
- Mehrere PC / Laptop / Pi
- VLANs 10 / 20 / 30
  - DMZ in VLAN 20
  - Client Netz in VLAN 10
  - Uplink zum Provider VLAN 30
- Router auf OpenWRT Basis
- Firewallregeln
  - Die Clients dürfen nur über Port 80, 443 nach draußen kommunizieren
  - SSH-Dienst nur auf dem Rechner in der DMZ und lokal ins Internet blockiert
  - Von der DMZ und aus dem Internet kein SSH ins lokale Netz
- Router on a Stick / Inter-VLAN-Routing

Konkretisierend zur obigen Beschreibung, sollen folgende Dienste und Funktionen umgesetzt werden:

- OpenWRT auf entweder einem Raspberry Pi oder auf einem Home-Router installieren
- Einrichtung von drei V-LANs (10, 20, 30)
- DHCP auf einem Raspberry Pi oder anderer Hardware einrichten
- DNS auf einem Raspberry Pi oder anderer Hardware einrichten
- Webserver auf einem Raspberry Pi oder anderer Hardware einrichten
- Firewall auf einem Raspberry Pi oder anderer Hardware einrichten
- Firewallregeln einrichten
- DMZ einrichten
- IPv4 Subnetzrechner konfigurieren
  - Dieser soll zu einer IP-Adresse mit Subnetzmaske, die Netz-ID und sie Anzahl der möglichen Clients berechnen

## Projektplanung

Die SUPER-IT Solutions GmbH soll die Vorgaben von Grund auf umsetzen.  
Die dafür benötigte Hardware wird von der GEEK-Fitness GmbH gestellt und kann dafür vollumfänglich genutzt werden.  
Ein möglicher Aufbau wurde von der SUPER-IT Solutions im Vorfeld konzipiert.

![netzwerkskizze](./src/netzwerkskizze.png)

Zunächst wird mit folgender Hardware geplant:

- 2x Raspberry Pi inkl. Strom -/ Patchkabel
- 2x Clientrechner inkl. Strom -/ Patchkabel
- 1x Switch mit mindestens sechs Ports inkl. Stromkabel

Zunächst sollen mit der Open-Source-Software "OpenWRT" sowohl Router und Firewall als auch DHCP und DNS auf dem "Raspbi 1#" installiert werden und dieser dann mit einem Patchkabel an den Port 2 des Switches angebunden werden.

Als nächstes wird auf dem "Raspbi 2#" ein Webserver installiert und dieser ebenfalls an den Switch angeschlossen. "Raspbi 2#" soll sich am Ende in einer DMZ (demilitarisierte Zone) befinden und von außerhalb des lokalen Netzes erreichbar sein.
Des Weiteren liegt hier auch der IPv4 Rechner.

Auf "Raspbi 3#" sollen die Dienste "DNS" und "DHCP" eingerichtet werden. "Raspbi 3#" soll dann zusammen mit den beiden Clientrechnern in eins von insgesamt drei geplanten "V-LANs".

Die beiden Clientrechner werden im final eingerichteten Netzwerk mit "Tools" bestückt und für den Endnutzer eingerichtet. Diese "Tools" enthalten z.B. einen IPv4-Rechner.

### Software -/ Betriebssystemwahl

Wir werden mit dem Betriebssystem "OpenWRT" arbeiten. Es handelt sich hierbei um ein Open-Source-Betriebssystem, das sowohl DHCP- und DNS-, als auch Firewall- und Routerfunktionalität besitzt.

## Projektrealisierung

### Installation und Konfiguration der Hardware

Den Switch konfigurieren wir mittels Konsolenkabel und der Software "PuTTY".  
Der Hostname lautet (ohne Anführungszeichen) "Switch" und das Passwort "cisco".  
Der Switch wird in drei "VLANs" unterteilt:

- VLAN 10 / Name "Clients" / Ports 5 / 6 / 7
- VLAN 20 / Name "DMZ" / Port 3
- VLAN 30 / Name "Uplink" / Port 8
- (Management VLAN 1 / Port 1 / 2 / 4)

Die zwei Raspberry Pi statten wir jeweils mit einer leeren SD-Karte aus.  
Diese werden im folgenden Abschnitt konfiguriert.

### Installation und Konfiguration der Software

Auf "Raspi1" wird "OpenWRT" installiert und DHCP / DNS / Firewall konfiguriert.

Bei "OpenWRT" handelt es sich um ein Linux-Betriebssystem. Dieses installieren wir auf dem "Raspbi1" aus und konfigurieren die Router- und Firewallfunktionen mittels der Weboberfläche des Betriebssystems.

Auf "Raspbi2" installieren wir ein UNIX-basiertes Betriebssystem und nutzen den Befehl (ohne Anführungszeichen) "sudo apt install nginx -y", um den Webserver "nginx" auf dem Raspberry Pi zu installieren.  
Des Weiteren platzieren wir auf diesem Raspberry Pi den IPv4 Rechner, der innerhalb des Netzwerks unter der IP 69.4.20.5 erreichbar ist.  
Die genaue [Dokumentation zum IPv4 Rechner](https://it2z-best-group.gitlab.io/lernfeld3/ipv4-rechner) ist unter über GitLab-Pages veröffentlicht.

## Anhang

### Quellenverzeichnis

- [openwrt.org](https://openwrt.org/)
- [netadmintools.com/how-to-configure-cisco-switches](https://www.netadmintools.com/how-to-configure-cisco-switches/)
- [docs.nginx.com](https://docs.nginx.com/)

### Grafiken

![netzwerkskizze](./src/netzwerkskizze.png)
